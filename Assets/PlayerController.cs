﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

[CreateAssetMenu( fileName = "PlayerController", menuName = "PlayerController", order = 0 )]
public class PlayerController : Controller
{
    public InputUser user;
    public Controls.GameplayActions gameplayActions;

    public override void UpdateController( ref InputState inputState)
    {
        inputState.down = gameplayActions.Down.phase == InputActionPhase.Started;
        inputState.left = gameplayActions.Left.phase == InputActionPhase.Started;
        inputState.right = gameplayActions.Right.phase == InputActionPhase.Started;
        inputState.rotCW = gameplayActions.RotateCW.triggered;
        inputState.rotCCW = gameplayActions.RotateCCW.triggered;
    }

    public override void Destruct()
    {
        user.UnpairDevices();
    }
}
