using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Users;
using TMPro;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

// DONE uses non-standard "locking soft drop". It should not lock, but then allow "first frame locking".
// TODO Use time delays from the NES/Gameboy versions
// TODO wall-kicks
// TODO keep score
// TODO increase the level, with speed and color changes


/*
Level 	Frames per Gridcell
00 	48
01 	43
02 	38
03 	33
04 	28
05 	23
06 	18
07 	13
08 	8
09 	6
10–12 	5
13–15 	4
16–18 	3
19–28 	2
29+ 	1  

(rounded: (FpG / 60fps) * 100Hz
00 80
01 72
02 63
03 55
04 47
05 38
06 30
07 22
08 13
09 10
10-12 8
13–15 7
16–18 5	
19–28 3
29+ 	2  
*/

/*
https://tetris.wiki/Tetris_Guideline

https://tetris.wiki/Tetris_%26_Dr._Mario
*** based on Tetris and Dr.Mario version (1994, SNES) ***

- uses Gameboy/Nintendo (non-SRS) rotations (is there a name for this system?)
- garbage lines are aligned in 10 intervals
- currently no wall kicking
- soft drop only
- no ghost piece


*/

// done: throttle downward speed
// done: press down when bottom is blocked -> lock immediately
// done: show next piece
// done: death condition
// done: sounds: bust a 4-liner
// done use UnityEngine.InputSystem, support local multiplayer
// done: use Timer class
// done: busted lines go to next player: only contiguous!
// done show points and seqIndex on HUD

public class Global : MonoBehaviour
{
  // NES values, scaled to 100Hz
  int[] gravity = {80, 72, 63, 55, 47, 38, 30, 22, 13, 10, 
    8, 8, 8, 
    7, 7, 7, 
    5, 5, 5, 
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    2 // 29
  };

  public static Global instance;

  public GameObject GamePrefab;

  [SerializeField] GameObject GameOverScreen;
  [SerializeField] GameObject JoinScreen;
  [SerializeField] GameObject ReadyScreen;
  [SerializeField] TMP_Text playerWinnerNotice;
  [SerializeField] Camera camera;
  // [SerializeField] PixelPerfectCamera ppc;
  Controls Controls;

  [SerializeField] int MaxPlayers = 4;
  [SerializeField] int paddingBetweenPlayers = 3;
  [SerializeField] int paddingVertical = 3;

  public enum Mode
  {
    Join,
    WaitingToStart,
    Gameplay,
    Paused,
    Gameover
  }

  public Mode mode = Mode.Join;

  private List<Game> games;
  private List<Game> activeGames;
  private List<Game> newGames;

  [SerializeField] AudioSource music;
  [SerializeField] AudioSource audioSource;

  public PlayerController SOpc;
  public AIController[] aiControllers;
  private int AI_Index;
  // public GameSettings gameSettings;

  // https://tetris.wiki/DAS
  [FormerlySerializedAs( "initialHoldSkip" )]
  public int DAS_initialHoldSkip = 20;

  public int holdSkip = 10;
  public int holdDownSkip = 5;
  public int bustSkip = 3;
  public int bumpSkip = 5;

  public int Seed = 0;
  public int TEST_CASE = 0;

  public int[] randomBlockSequence;
  public Block[] blocks = new Block[7];

  public int[] randomEmptyColumnSequence;

  public bool quick = false;

  Vector3 camMoveTarget;
  public float orthoTarget;
  public float camSpeed = 1;
  public float orthoSpeed = 1;
  public float floatySpeed = 5;


  void Awake()
  {
    if( instance != null )
    {
      Destroy( gameObject );
      return;
    }
    instance = this;
    DontDestroyOnLoad( gameObject );

    randomBlockSequence = new int[10000];
    randomEmptyColumnSequence = new int[20];
    Random.InitState( Seed );
    for( int i = 0; i < randomBlockSequence.Length; i++ )
      randomBlockSequence[i] = Random.Range( 0, blocks.Length );
    if( TEST_CASE == 0 )
      randomBlockSequence[0] = 3; // Z
    if( TEST_CASE == 1 )
    {
      randomBlockSequence[0] = 1; // J
      randomBlockSequence[1] = 5; // I
    }

    for( int i = 0; i < randomEmptyColumnSequence.Length; i++ )
      randomEmptyColumnSequence[i] = Random.Range( 0, 10 );
    Block blockL = new Block();
    blocks[0] = blockL;
    blockL.blockID = 0;
    blockL.colorID = 0;
    blockL.rotation = new Vector2Int[4][];
    blockL.rotation[0] = new[] {new Vector2Int( 1, 0 ), new Vector2Int( 0, 0 ), new Vector2Int( -1, 0 ), new Vector2Int( -1, -1 )};
    blockL.rotation[1] = new[] {new Vector2Int( -1, 1 ), new Vector2Int( 0, 1 ), new Vector2Int( 0, 0 ), new Vector2Int( 0, -1 )};
    blockL.rotation[2] = new[] {new Vector2Int( 1, 1 ), new Vector2Int( 1, 0 ), new Vector2Int( 0, 0 ), new Vector2Int( -1, 0 )};
    blockL.rotation[3] = new[] {new Vector2Int( 0, 1 ), new Vector2Int( 0, 0 ), new Vector2Int( 0, -1 ), new Vector2Int( 1, -1 )};

    Block blockJ = new Block();
    blocks[1] = blockJ;
    blockJ.blockID = 1;
    blockJ.colorID = 1;
    blockJ.rotation = new Vector2Int[4][];
    blockJ.rotation[0] = new[] {new Vector2Int( 1, -1 ), new Vector2Int( 1, 0 ), new Vector2Int( 0, 0 ), new Vector2Int( -1, 0 )};
    blockJ.rotation[1] = new[] {new Vector2Int( 0, 1 ), new Vector2Int( 0, 0 ), new Vector2Int( 0, -1 ), new Vector2Int( -1, -1 )};
    blockJ.rotation[2] = new[] {new Vector2Int( -1, 1 ), new Vector2Int( -1, 0 ), new Vector2Int( 0, 0 ), new Vector2Int( 1, 0 )};
    blockJ.rotation[3] = new[] {new Vector2Int( 1, 1 ), new Vector2Int( 0, 1 ), new Vector2Int( 0, 0 ), new Vector2Int( 0, -1 )};

    Block blockO = new Block();
    blocks[2] = blockO;
    blockO.blockID = 2;
    blockO.colorID = 2;
    blockO.rotation = new Vector2Int[1][];
    blockO.rotation[0] = new[] {new Vector2Int( 0, 0 ), new Vector2Int( -1, 0 ), new Vector2Int( -1, -1 ), new Vector2Int( 0, -1 )};

    Block blockZ = new Block();
    blocks[3] = blockZ;
    blockZ.blockID = 3;
    blockZ.colorID = 1;
    blockZ.rotation = new Vector2Int[2][];
    blockZ.rotation[0] = new[] {new Vector2Int( -1, 0 ), new Vector2Int( 0, 0 ), new Vector2Int( 0, -1 ), new Vector2Int( 1, -1 )};
    blockZ.rotation[1] = new[] {new Vector2Int( 0, -1 ), new Vector2Int( 0, 0 ), new Vector2Int( 1, 0 ), new Vector2Int( 1, 1 )};

    Block blockS = new Block();
    blocks[4] = blockS;
    blockS.blockID = 4;
    blockS.colorID = 0;
    blockS.rotation = new Vector2Int[2][];
    blockS.rotation[0] = new[] {new Vector2Int( 1, 0 ), new Vector2Int( 0, 0 ), new Vector2Int( 0, -1 ), new Vector2Int( -1, -1 )};
    blockS.rotation[1] = new[] {new Vector2Int( 0, 1 ), new Vector2Int( 0, 0 ), new Vector2Int( 1, 0 ), new Vector2Int( 1, -1 )};

    Block blockI = new Block();
    blocks[5] = blockI;
    blockI.blockID = 5;
    blockI.colorID = 3;
    blockI.rotation = new Vector2Int[2][];
    blockI.rotation[0] = new[] {new Vector2Int( -2, 0 ), new Vector2Int( -1, 0 ), new Vector2Int( 0, 0 ), new Vector2Int( 1, 0 )};
    blockI.rotation[1] = new[] {new Vector2Int( 0, -1 ), new Vector2Int( 0, 0 ), new Vector2Int( 0, 1 ), new Vector2Int( 0, 2 )};

    Block blockT = new Block();
    blocks[6] = blockT;
    blockT.blockID = 6;
    blockT.colorID = 2;
    blockT.rotation = new Vector2Int[4][];
    blockT.rotation[0] = new[] {new Vector2Int( 0, 0 ), new Vector2Int( -1, 0 ), new Vector2Int( 1, 0 ), new Vector2Int( 0, -1 )};
    blockT.rotation[1] = new[] {new Vector2Int( 0, 0 ), new Vector2Int( -1, 0 ), new Vector2Int( 0, 1 ), new Vector2Int( 0, -1 )};
    blockT.rotation[2] = new[] {new Vector2Int( 0, 0 ), new Vector2Int( -1, 0 ), new Vector2Int( 1, 0 ), new Vector2Int( 0, 1 )};
    blockT.rotation[3] = new[] {new Vector2Int( 0, 0 ), new Vector2Int( 0, 1 ), new Vector2Int( 0, -1 ), new Vector2Int( 1, 0 )};

    ReadyScreen.SetActive( false );
    GameOverScreen.SetActive( false );
    JoinScreen.SetActive( true );
    PauseScreen.SetActive( false );
    playerWinnerNotice.gameObject.SetActive( false );

    games = new List<Game>();
    newGames = new List<Game>();
    activeGames = new List<Game>();

    Controls = new Controls();
    Controls.Enable();
    Controls.Global.StartGame.performed += context =>
    {
      if( mode == Mode.Join )
        StartGame();
    };
    Controls.Global.Pause.performed += context =>
    {
      if( mode == Mode.Paused )
        Unpause();
      else if( mode == Mode.Gameplay )
        Pause();
    };
    Controls.Global.AddAIplayer.performed += context => { AddAI(); };
    Controls.Global.Reset.performed += context => { Reset(); };
    Controls.Global.ChangeDifficulty.performed += context =>
    {
      if( mode != Mode.Join )
        return;
      System.Nullable<InputUser> user = InputUser.FindUserPairedToDevice( context.control.device );
      foreach( var player in games )
        if( player.controller is PlayerController && ((PlayerController) player.controller).user == user )
        {
          player.difficultyLevel = player.difficultyLevel + (int) (context.ReadValue<float>());
          player.UIDifficulty.text = player.difficultyLevel.ToString();
          break;
        }
    };

    InputUser.onUnpairedDeviceUsed += OnUnpairedDeviceUsed;
    BeginJoining();

    // testing
    // AddAI();
    // EndJoining();
    // StartGame();
  }

  private void OnDestroy()
  {
    InputUser.onUnpairedDeviceUsed -= OnUnpairedDeviceUsed;
    InputUser.onUnpairedDeviceUsed -= OnUnpairedDeviceUsed;
  }

  void OnUnpairedDeviceUsed( InputControl control, InputEventPtr eventPtr )
  {
    // Ignore anything but button presses.
    if( !(control is ButtonControl) )
      return;
    Debug.Log( "Unpaired device detected" + control.device.displayName );

    // get a new InputUser, now paired with the device
    InputUser user = InputUser.PerformPairingWithDevice( control.device );
    // Create a new instance of input actions to prevent InputUser from triggering actions on another InputUser.
    Controls controlsForThisUser = new Controls();
    // you must enable the controls to use them
    controlsForThisUser.Enable();
    // the real work is done for us in InputUser
    user.AssociateActionsWithUser( controlsForThisUser );

    // Create a new game object
    Game game = AddNewGame();

    PlayerController pc = ScriptableObject.Instantiate( SOpc );
    // store the InputUser so you can unpair later
    pc.user = user;
    // initialize your script with the new controls
    pc.gameplayActions = controlsForThisUser.Gameplay;

    pc.Initialize( game );
    game.Initialize( OnDeath, pc );

    InputUser.listenForUnpairedDeviceActivity--;
    if( InputUser.listenForUnpairedDeviceActivity == 0 )
    {
      EndJoining();
      ReadyScreen.SetActive( true );
    }
  }

  void BeginJoining()
  {
    mode = Mode.Join;
    InputUser.listenForUnpairedDeviceActivity = MaxPlayers;
    JoinScreen.SetActive( true );
  }

  void EndJoining()
  {
    mode = Mode.WaitingToStart;
    InputUser.listenForUnpairedDeviceActivity = 0;
    JoinScreen.SetActive( false );
  }

  Game AddNewGame()
  {
    GameObject go = Instantiate( GamePrefab );
    Game game = go.GetComponent<Game>();
    // You cannot do things like set the parent transform in the device pairing callback, so
    // add the game object to a list of new players for processing later.
    newGames.Add( game );
    Vector3 moveTarget = new Vector3( (newGames.Count + activeGames.Count - 1) * (game.dimension.x + paddingBetweenPlayers), 0, 0 );
    go.transform.position = moveTarget + Vector3.up * 10;
    game.moveTarget = moveTarget;
    game.moveSpeed = floatySpeed;
    if( mode != Mode.Gameplay )
    {
      ProcessNewPlayers();
      camera.transform.position = camMoveTarget;
      camera.orthographicSize = orthoTarget;
    }
    return game;
  }

  void AddAI()
  {
    if( mode != Mode.Join && mode != Mode.Gameplay )
      return;
    Game game = AddNewGame();
    AIController aiController = ScriptableObject.Instantiate( aiControllers[AI_Index] );
    aiController.name = aiController.name.Substring( 0, aiController.name.IndexOf( '(' ) );
    aiController.Initialize( game );
    game.gameObject.name = "AI " + AI_Index;
    game.UIDisplayName.text = aiController.name;
    game.Initialize( OnDeath, aiController );
    AI_Index = (AI_Index + 1) % aiControllers.Length;
  }

  public Game GetNextLivingPlayer( Game game )
  {
    int index = -1;
    for( int i = 0; i < activeGames.Count; i++ )
    {
      if( game.GetInstanceID() == activeGames[i].GetInstanceID() )
      {
        index = i;
        break;
      }
    }
    if( index < 0 )
      return null;
    int nextIndex = (index + 1) % activeGames.Count;
    if( nextIndex != index )
      return activeGames[nextIndex];
    return null;
  }

  void OnDeath( Game dead )
  {
    activeGames.Remove( dead );
    if( activeGames.Count <= 1 )
    {
      if( games.Count == 1 )
        GameOver( null );
      else
        for( int i = 0; i < games.Count; i++ )
          if( !games[i].isDead )
            GameOver( games[i] );
    }
    Vector3 start = dead.gameObject.transform.position;
    new Timer( dead, 3, (timer =>
    {
      dead.gameObject.transform.localScale = Vector3.one * (1f - timer.ProgressNormalized);
      dead.gameObject.transform.position = start + Vector3.up * Mathf.SmoothStep( 0, -50, timer.ProgressNormalized );
    }), () =>
    {
      games.Remove( dead );
      Destroy( dead.gameObject );
      RepositionEverything();
    } );
  }

  void GameOver( Game winner )
  {
    mode = Mode.Gameover;
    GameOverScreen.SetActive( true );
    // ugly: find winning player index 
    if( winner != null )
    {
      int winnerIndex = 0;
      for( int i = 0; i < games.Count; i++ )
        if( games[i].GetInstanceID() == winner.GetInstanceID() )
        {
          winnerIndex = i + 1;
          break;
        }
      playerWinnerNotice.gameObject.SetActive( true );
      playerWinnerNotice.text = "player " + winnerIndex + " wins!";
    }
    else
    {
      playerWinnerNotice.gameObject.SetActive( false );
    }
    Controls.Disable();
    new Timer( this, 2, null, () => { Controls.Enable(); } );
  }

  void Reset()
  {
    if( mode == Mode.Paused )
      Unpause();
    mode = Mode.Join;
    GameOverScreen.SetActive( false );
    foreach( var player in games )
      Destroy( player.gameObject );
    games.Clear();
    activeGames.Clear();
    BeginJoining();
    AI_Index = 0;
  }

  void StartGame()
  {
    if( mode == Mode.Join )
      EndJoining();
    mode = Mode.Gameplay;
    GameOverScreen.SetActive( false );
    ReadyScreen.SetActive( false );
    if( quick )
      Time.fixedDeltaTime = 0.001f;
    else
      Time.fixedDeltaTime = 0.01f;
    // ProcessNewPlayers();
    // camera.transform.position = camMoveTarget;
    // camera.orthographicSize = orthoTarget;
  }

  void RepositionEverything()
  {
    float x = transform.position.x;
    float height = 0;
    float width = 0;
    for( int i = 0; i < activeGames.Count; i++ )
    {
      if( x > 0 )
        x += paddingBetweenPlayers;

      Game game = activeGames[i];
      game.moveTarget = new Vector3( x, 0, 0 );
      game.moveSpeed = floatySpeed;

      x += activeGames[i].dimension.x;
      height = Mathf.Max( activeGames[i].dimension.y, height );
      width = x;
    }

    camMoveTarget = new Vector3( x * 0.5f, height * 0.5f, -10 );
    //ppc.assetsPPU = 11 - Mathf.Max( 0, players.Count - 2 );
    orthoTarget = Mathf.Max( height * 0.5f, width * 0.5f / camera.aspect ) + paddingVertical;
  }

  void ProcessNewPlayers()
  {
    foreach( var game in newGames )
    {
      game.transform.parent = transform;
      games.Add( game );
      activeGames.Add( game );
    }
    if( newGames.Count > 0 )
      RepositionEverything();
    newGames.Clear();
  }

  void FixedUpdate()
  {
    if( mode == Mode.Gameplay )
    {
      ProcessNewPlayers();
      for( int i = 0; i < activeGames.Count; i++ )
        if( !activeGames[i].isDead )
          activeGames[i].ManualUpdate();
    }
  }

  void Update()
  {
    Timer.UpdateTimers();
    camera.transform.position = Vector3.MoveTowards( camera.transform.position, camMoveTarget, camSpeed * Time.deltaTime );
    camera.orthographicSize = Mathf.MoveTowards( camera.orthographicSize, orthoTarget, orthoSpeed * Time.deltaTime );
  }

  public void PlaySound( AudioClip clip )
  {
    if( !quick )
      audioSource.PlayOneShot( clip );
  }

  public GameObject PauseScreen;

  void Pause()
  {
    mode = Mode.Paused;
    PauseScreen.SetActive( true );
  }

  void Unpause()
  {
    mode = Mode.Gameplay;
    PauseScreen.SetActive( false );
  }
}