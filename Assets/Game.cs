﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Tilemaps;

[System.Serializable]
public class Controller : ScriptableObject
{
  protected Game game { get; set; }

  public virtual void Initialize( Game game )
  {
    this.game = game;
  }

  public virtual void Destruct() { }

  public virtual void UpdateController( ref InputState inputState ) { }

  public virtual void OnNewBlock( Block newBlock ) { }
}

// Controls the board with player input or AI using this struct
public struct InputState
{
  public bool right;
  public bool left;
  public bool down;
  public bool rotCW;
  public bool rotCCW;
}

// this is a player's controllable game piece
public class Block
{
  public Block() { }

  public Block( Block other )
  {
    rotation = new Vector2Int[other.rotation.Length][];
    for( int i = 0; i < other.rotation.Length; i++ )
    {
      rotation[i] = new Vector2Int[other.rotation[i].Length];
      System.Array.Copy( other.rotation[i], rotation[i], other.rotation[i].Length );
    }
    colorID = other.colorID;
    blockID = other.blockID;
  }

  //public int blockID;
  public Vector2Int pos;
  public int rotationID;
  // [ rotationID ][ array of cell locations ]
  public Vector2Int[][] rotation;
  public int colorID;
  public int blockID;
}

// BlockState bundles the position and rotation associated with a block without affecting the block's internal state.
// Used to query the map for collisions, and for pathing.
public struct BlockState
{
  public BlockState( BlockState rhs )
  {
    block = rhs.block;
    pos = rhs.pos;
    rot = rhs.rot;
  }

  public BlockState( Block b )
  {
    block = b;
    pos = b.pos;
    rot = b.rotationID;
  }

  public Block block;
  public Vector2Int pos;
  public int rot;
}

public struct Cell
{
  public bool occupied;
  public int colorID;
  public int tileID;
}


public struct GameState
{
  public Cell[,] cells;

  public GameState( Vector2Int dimension )
  {
    // add cells on top to allow block to rotate immediately
    // without hitting the ceiling
    cells = new Cell[dimension.x, dimension.y + 5];
    for( int x = 0; x < dimension.x; x++ )
    for( int y = 0; y < dimension.y; y++ )
    {
      Cell cell = new Cell();
      cell.occupied = false;
      cell.colorID = 0;
      cells[x, y] = cell;
    }
  }
}

public class Game : MonoBehaviour
{
  public int difficultyLevel;
  // 80 is easy, 20- is hard
  public int stepSkip = 80;

  public GameState currentState;

  [SerializeField] public Controller controller;

  [SerializeField] public LineRenderer lineRenderer;

  public Vector2Int dimension = new Vector2Int( 10, 20 );

  private System.Action<Game> OnDeath;

  [SerializeField] Color[] colors;
  [SerializeField] AudioClip audioMove;
  [SerializeField] AudioClip audioRotate;
  [SerializeField] AudioClip audioLand;
  [SerializeField] AudioClip audioBust;
  [SerializeField] AudioClip audioBustFour;
  [SerializeField] AudioClip audioBump;
  [SerializeField] AudioClip audioDeath;

  // there are four tile colors for a scheme
  [SerializeField] public Tilemap tilemapLocked;
  [SerializeField] Tilemap tilemapFree;
  [SerializeField] Tilemap tilemapNextBlock;
  [SerializeField] Tilemap tilemapBorderwall;
  [SerializeField] Tile tileFree;
  [SerializeField] Tile tileBackground;
  [SerializeField] Tile tileLocked;
  [SerializeField] Tile tileWall;
  Tile[] freeTileColored;
  Tile[] lockedTileColored;

  public Block activeBlock;
  public Block nextBlock;
  public Vector2Int startPos;
  public int seqIndex = 0;
  private bool movedDownThisFrame = false;

  private int step;
  private int lockDelay;
  private int hold;
  private int holdDown;
  private int bustCountdown;
  private int bumpCount;
  // used to play the add-lines sound only once at a time
  private bool bumpFlag = true;

  public class Buster
  {
    public BoundsInt bounds;
    public int countdown;
  }

  List<Buster> bustList = new List<Buster>();

  public class AddLineParams
  {
    public int startY;
    public int count;
    public int emptyColumn;
  }

  List<AddLineParams> addLineQueue = new List<AddLineParams>();

  // for adding lines to opponents
  private int continguousBustCount;

  private const int ContiguousRowsWithSameEmptyColumn = 10;
  private int switchEmptyColumnCounter = ContiguousRowsWithSameEmptyColumn;
  int emptyColumn = 0;
  private int randomEmptyColumnSequenceIndex = 0;

  InputState inputState;
  InputState previousInputState;
  public bool isDead;
  public Vector3Int boxsize = new Vector3Int( 4, 4, 0 );

  [SerializeField] private TextMeshProUGUI UISeqIndex;
  [SerializeField] private TextMeshProUGUI UIScore;
  [SerializeField] private TextMeshProUGUI UILines;
  [SerializeField] private TextMeshProUGUI UIHoles;
  [SerializeField] private TextMeshProUGUI UILowest;
  [SerializeField] private TextMeshProUGUI UIRow;
  [SerializeField] private TextMeshProUGUI UICol;

  [SerializeField] public TextMeshProUGUI UIDifficulty;
  [SerializeField] public TextMeshProUGUI UIDisplayName;

  public SpriteRenderer rotateSprite;
  public RotSprite[] RotSprites;

  [System.Serializable]
  public struct RotSprite
  {
    public Sprite[] CW;
  }

  public void UpdateUI( int seqIndex, float score, int lines, int holes, int lowest, int row, int col )
  {
    UISeqIndex.text = "" + seqIndex;
    UIScore.text = "" + score;
    UILines.text = "" + lines;
    UIHoles.text = "" + holes;
    UILowest.text = "" + lowest;
    UIRow.text = "" + row;
    UICol.text = "" + col;
  }

  private void OnDestroy()
  {
    controller.Destruct();
  }

  public void Initialize( System.Action<Game> onDeath, Controller controller )
  {
    this.controller = controller;
    OnDeath = onDeath;
    inputState = default;
    previousInputState = inputState;
    currentState = new GameState( dimension );
    startPos = new Vector2Int( dimension.x / 2, dimension.y - 1 );
    emptyColumn = Global.instance.randomEmptyColumnSequence[0];

    freeTileColored = new Tile[5];
    freeTileColored[0] = Instantiate( tileFree );
    freeTileColored[0].color = colors[0];
    freeTileColored[1] = Instantiate( tileFree );
    freeTileColored[1].color = colors[1];
    freeTileColored[2] = Instantiate( tileFree );
    freeTileColored[2].color = colors[2];
    freeTileColored[3] = Instantiate( tileFree );
    freeTileColored[3].color = colors[3];
    freeTileColored[4] = Instantiate( tileFree );
    freeTileColored[4].color = colors[4];

    lockedTileColored = new Tile[5];
    lockedTileColored[0] = Instantiate( tileLocked );
    lockedTileColored[0].color = colors[0];
    lockedTileColored[1] = Instantiate( tileLocked );
    lockedTileColored[1].color = colors[1];
    lockedTileColored[2] = Instantiate( tileLocked );
    lockedTileColored[2].color = colors[2];
    lockedTileColored[3] = Instantiate( tileLocked );
    lockedTileColored[3].color = colors[3];
    lockedTileColored[4] = Instantiate( tileLocked );
    lockedTileColored[4].color = colors[4];

    //tilemapLocked.BoxFill( new Vector3Int( 0, 0, -1 ), tileBackground, -1, -1, dimension.x, dimension.y );
    tilemapNextBlock.transform.position = tilemapLocked.transform.position + new Vector3( dimension.x / 2 - boxsize.x / 2, dimension.y - 1, 0 );
    tilemapNextBlock.size = new Vector3Int( 4, 4, 0 );
    tilemapNextBlock.ResizeBounds();

    // floor
    for( int i = -1; i < dimension.x + 1; i++ )
      tilemapBorderwall.SetTile( new Vector3Int( i, -1, 1 ), tileWall );
    // top: no solid wall at the top so that big blocks can rotate immediately
    for( int i = -1; i < dimension.x / 2 - boxsize.x / 2; i++ )
      tilemapBorderwall.SetTile( new Vector3Int( i, dimension.y, 1 ), tileWall );
    for( int i = dimension.x / 2 + boxsize.x / 2; i < dimension.x + 1; i++ )
      tilemapBorderwall.SetTile( new Vector3Int( i, dimension.y, 1 ), tileWall );
    // wall left
    for( int i = 0; i < dimension.y + 1; i++ )
      tilemapBorderwall.SetTile( new Vector3Int( -1, i, 1 ), tileWall );
    // wall right
    for( int i = 0; i < dimension.y + 1; i++ )
      tilemapBorderwall.SetTile( new Vector3Int( dimension.x, i, 1 ), tileWall );

    Reset();
  }

  public void Reset()
  {
    isDead = false;
    seqIndex = 0;

    step = stepSkip;
    hold = Global.instance.holdSkip;
    holdDown = Global.instance.DAS_initialHoldSkip;
    bustCountdown = Global.instance.bustSkip;
    lockDelay = stepSkip;

    nextBlock = new Block( Global.instance.blocks[Global.instance.randomBlockSequence[0]] );
    nextBlock.pos = new Vector2Int( 2, 2 );
    for( int i = 0; i < boxsize.x * boxsize.y; i++ )
      tilemapNextBlock.SetTile( new Vector3Int( i % boxsize.x, i / boxsize.x, 0 ), tileBackground );
    SetTilesDisplay( tilemapNextBlock, nextBlock, freeTileColored );
    rotateSprite.gameObject.SetActive( false );

    if( Global.instance.TEST_CASE == 1 )
    {
      for( int y = 0; y < dimension.y; y++ )
      for( int x = 0; x < dimension.x; x++ )
      {
        currentState.cells[x, y].occupied = y < 5 && x > 0;
        currentState.cells[x, y].colorID = 0;
      }
    }
    else if( Global.instance.TEST_CASE == 2 )
    {
      currentState.cells[0, 0].occupied = true;
      currentState.cells[1, 0].occupied = true;
      currentState.cells[4, 0].occupied = true;
      currentState.cells[3, 1].occupied = true;
      currentState.cells[4, 1].occupied = true;
    }
    else if( Global.instance.TEST_CASE == 3 )
    {
      for( int y = 0; y < dimension.y; y++ )
      for( int x = 0; x < dimension.x; x++ )
      {
        currentState.cells[x, y].occupied = (y < 2 && x > 0) || (y < 4 && x > 1);
        currentState.cells[x, y].colorID = 0;
      }
    }

    UpdateTilemap( ref currentState );
  }

  public Vector3 moveTarget;
  public float moveSpeed;

  void Update()
  {
    if( moveSpeed > 0.00001f )
      transform.position = Vector3.MoveTowards( transform.position, moveTarget, moveSpeed * Time.deltaTime );
    if( Vector3.SqrMagnitude( transform.position - moveTarget ) < 0.00001f )
    {
      transform.position = moveTarget;
      moveSpeed = 0;
    }
  }

  public void ManualUpdate()
  {
    stepSkip = Mathf.Max( 1, 100 - difficultyLevel * 9 );
    UIDifficulty.text = difficultyLevel.ToString();

    if( bustList.Count > 0 )
    {
      // animate the busting of lines
      if( bustCountdown-- == 0 )
      {
        bustCountdown = Global.instance.bustSkip;
        for( int i = 0; i < bustList.Count; i++ )
        {
          Buster bust = bustList[i];
          bust.countdown--;
          currentState.cells[bust.bounds.min.x + bust.countdown / 2, bust.bounds.min.y].occupied = false;
          currentState.cells[bust.bounds.max.x - bust.countdown / 2 - 1, bust.bounds.min.y].occupied = false;
          if( bust.countdown <= 0 )
          {
            bustList.RemoveAt( i );
            i--;
          }
        }
      }

      if( bustList.Count == 0 )
      {
        for( int i = 0; i < dimension.y; i++ )
          GravityStep();
        if( continguousBustCount > 1 )
        {
          Game next = Global.instance.GetNextLivingPlayer( this );
          if( next != null )
            next.QueueLines( new AddLineParams {startY = 0, count = continguousBustCount, emptyColumn = 0} );
        }
        continguousBustCount = 0;
      }
    }
    else if( activeBlock == null )
    {
      if( addLineQueue.Count > 0 )
      {
        /*
        foreach( var als in addLineQueue )
          AddLines( als );
        addLineQueue.Clear();
        */
        if( bumpFlag )
        {
          bumpFlag = false;
          Global.instance.PlaySound( audioBump );
        }
        if( bumpCount-- == 0 )
        {
          bumpCount = Global.instance.bumpSkip;
          addLineQueue[0].count--;
          if( addLineQueue[0].count == 0 )
            addLineQueue.RemoveAt( 0 );
          AddLines( new AddLineParams() {count = 1, emptyColumn = 0, startY = 0} );
        }
        if( addLineQueue.Count == 0 )
          bumpFlag = true;
        UpdateTilemap( ref currentState );
      }
      else
      {
        activeBlock = nextBlock;
        activeBlock.pos = startPos;
        controller.OnNewBlock( activeBlock );

        SetTilesDisplay( tilemapFree, activeBlock, freeTileColored );
        if( IsBlocked( activeBlock, Vector2Int.zero ) )
        {
          isDead = true;
          OnDeath( this );
          Global.instance.PlaySound( audioDeath );
          return;
        }

        seqIndex = (seqIndex + 1) % Global.instance.randomBlockSequence.Length;
        nextBlock = new Block( Global.instance.blocks[Global.instance.randomBlockSequence[seqIndex]] );
        nextBlock.pos = new Vector2Int( 2, 2 );
        for( int i = 0; i < boxsize.x * boxsize.y; i++ )
          tilemapNextBlock.SetTile( new Vector3Int( i % boxsize.x, i / boxsize.x, 0 ), null );
        SetTilesDisplay( tilemapNextBlock, nextBlock, freeTileColored );
      }
    }
    else
    if( activeBlock!= null )
    {
      movedDownThisFrame = false;
      
      int previousRotationID = activeBlock.rotationID;
      
      controller.UpdateController( ref inputState );
      
      HandleInput( ref inputState );

      // "first frame locking". There is a delay after the block first lands,
      // during which the player can move the piece, or press down to lock immediately.
      if( activeBlock != null )
      {
        if( IsBlocked( activeBlock, Vector2Int.down ) )
        {
          if( lockDelay-- <= 0 )
          {
            lockDelay = stepSkip;
            LockActiveBlock();
          }
        }
        else
        {
          lockDelay = stepSkip;
        }
      }
      
      // use frame skipping with a fixed timestep instead of timers, for predictable movement.
      if( forceStep || step-- == 0 )
      {
        forceStep = false;
        step = stepSkip;
        Step();
      }
      
      tilemapFree.ClearAllTiles();

      if( activeBlock == null )
      {
        rotateSprite.gameObject.SetActive( false );
      }
      else
      {
        // sprite rotation (polish)
        // square's rotation cannot technically change
        if( activeBlock.rotationID != previousRotationID || (activeBlock.blockID == 2 && (inputState.rotCW || inputState.rotCCW)) )
        {
          rotateSprite.gameObject.SetActive( true );
          rotateSprite.gameObject.transform.position = (Vector2) transform.position + activeBlock.pos + Vector2.one * 0.5f;
          rotateSprite.color = colors[activeBlock.colorID];
          Sprite[] rots = RotSprites[activeBlock.blockID].CW;
          if( inputState.rotCW )
            rotateSprite.sprite = rots[(activeBlock.rotationID) % rots.Length];
          if( inputState.rotCCW )
            rotateSprite.sprite = rots[(activeBlock.rotationID - 1 + rots.Length) % rots.Length];
        }
        else
        {
          rotateSprite.gameObject.SetActive( false );
          SetTilesDisplay( tilemapFree, activeBlock, freeTileColored );
        }
      }

      inputState = default;
    }

    UpdateTilemap( ref currentState );
  }

  void MoveDownOne()
  {
    if( !movedDownThisFrame )
      activeBlock.pos += Vector2Int.down;
    movedDownThisFrame = true;
  }
  
  void Step()
  {
    GravityStep();
    
    if( activeBlock != null && !IsBlocked( activeBlock, Vector2Int.down ) )
      MoveDownOne();

    // commence preparations for busting lines!
    int contiguousBustedLines = 0;
    bool bustfour = false;
    for( int y = 0; y < dimension.y; y++ )
    {
      int count = 0;
      for( int x = 0; x < dimension.x; x++ )
        if( currentState.cells[x, y].occupied )
          count++;
        else
          break;
      if( count == dimension.x )
      {
        // bust it!
        Buster bust = new Buster();
        bust.bounds = new BoundsInt( 0, y, 0, dimension.x, 1, 0 );
        bust.countdown = dimension.x;
        bustList.Add( bust );
        bustCountdown = Global.instance.bustSkip;
        contiguousBustedLines++;
        if( contiguousBustedLines >= 4 )
          bustfour = true;
        continguousBustCount = Mathf.Max( continguousBustCount, contiguousBustedLines );
      }
      else
        contiguousBustedLines = 0;
    }

    if( bustList.Count > 0 )
    {
      Global.instance.PlaySound( audioBust );
      if( bustfour )
        Global.instance.PlaySound( audioBustFour );
    }
  }

  void MoveBlock( Block block, Vector2Int direction )
  {
    if( !IsBlocked( block, direction ) )
    {
      activeBlock.pos += direction;
      Global.instance.PlaySound( audioMove );
    }
  }

  void RotateBlock( Block block, int increment )
  {
    BlockState bs = new BlockState( block );
    bs.rot = (block.rotationID + increment + block.rotation.Length) % block.rotation.Length;
    if( !IsBlocked( bs, Vector2Int.zero ) )
    {
      block.rotationID = bs.rot;
      Global.instance.PlaySound( audioRotate );
    }
  }

  private bool forceStep;
  
  void LockActiveBlock()
  {
    WriteCells( activeBlock );
    activeBlock = null;
    forceStep = true;
    Global.instance.PlaySound( audioLand );
  }

  void GravityStep()
  {
    for( int y = 0; y < dimension.y; y++ )
    {
      bool isEmptyBelow = true;
      for( int x = 0; x < dimension.x; x++ )
      {
        if( y == 0 || currentState.cells[x, y - 1].occupied )
        {
          isEmptyBelow = false;
          break;
        }
      }

      if( isEmptyBelow )
      {
        for( int x = 0; x < dimension.x; x++ )
          currentState.cells[x, y - 1] = currentState.cells[x, y];
        for( int x = 0; x < dimension.x; x++ )
          currentState.cells[x, y].occupied = false;
      }
    }
  }


  public void QueueLines( AddLineParams als )
  {
    addLineQueue.Add( als );
  }

  private void AddLines( AddLineParams als )
  {
    // move everything upward
    for( int i = 0; i < als.count; i++ )
    for( int y = dimension.y - 1; y > als.startY; y-- )
    for( int x = 0; x < dimension.x; x++ )
      currentState.cells[x, y] = currentState.cells[x, y - 1];
    // add lines to the now-vacant space
    for( int y = 0; y < als.count; y++ )
    {
      switchEmptyColumnCounter--;
      if( switchEmptyColumnCounter == 0 )
      {
        switchEmptyColumnCounter = ContiguousRowsWithSameEmptyColumn;
        emptyColumn = Global.instance.randomEmptyColumnSequence[++randomEmptyColumnSequenceIndex];
      }
      for( int x = 0; x < dimension.x; x++ )
      {
        currentState.cells[x, als.startY + als.count - 1 - y].occupied = (x != emptyColumn);
        currentState.cells[x, als.startY + als.count - 1 - y].colorID = 4;
      }
    }
  }

  void HandleInput( ref InputState input )
  {
    if( activeBlock != null )
    {
      if( input.left )
      {
        if( previousInputState.left )
        {
          if( hold-- <= 0 )
          {
            hold = Global.instance.holdSkip;
            MoveBlock( activeBlock, Vector2Int.left );
          }
        }
        else
        {
          hold = Global.instance.DAS_initialHoldSkip;
          MoveBlock( activeBlock, Vector2Int.left );
        }
      }

      if( input.right )
      {
        if( previousInputState.right )
        {
          if( hold-- <= 0 )
          {
            hold = Global.instance.holdSkip;
            MoveBlock( activeBlock, Vector2Int.right );
          }
        }
        else
        {
          hold = Global.instance.DAS_initialHoldSkip;
          MoveBlock( activeBlock, Vector2Int.right );
        }
      }

      if( input.rotCCW )
        RotateBlock( activeBlock, -1 );

      if( input.rotCW )
        RotateBlock( activeBlock, 1 );

      // keep activeBlock == null statements after attempts to access, such as LockActiveBlock()
      if( input.down )
      {
        if( holdDown-- == 0 )
        {
          holdDown = Global.instance.holdDownSkip;
          if( !IsBlocked( activeBlock, Vector2Int.down ) )
            MoveDownOne();
          else
            LockActiveBlock();
        }
        
        /*if( !previousInputState.down && IsBlocked( activeBlock, Vector2Int.down ) )
            LockActiveBlock();
        else if( holdDown-- == 0 )
        {
          holdDown = Global.instance.holdDownSkip;
          if( !IsBlocked( activeBlock, Vector2Int.down ) )
            MoveDownOne();
        }*/
      }
    }

    previousInputState = input;
  }

  void UpdateTilemap( ref GameState state )
  {
    Vector3Int pos = Vector3Int.zero;
    Cell cell;
    for( int x = 0; x < dimension.x; x++ )
    for( int y = 0; y < dimension.y; y++ )
    {
      pos.x = x;
      pos.y = y;
      cell = state.cells[x, y];
      tilemapLocked.SetTile( pos, cell.occupied ? lockedTileColored[cell.colorID] : null );
    }
  }

  /*void WriteCells( BlockState bs, Cell[,] cells )
  {
    for( int i = 0; i < bs.block.rotation[bs.rot].Length; i++ )
    {
      Vector2Int pos = bs.pos + bs.block.rotation[bs.rot][i];
      if( pos.x < cells.GetLength( 0 ) && pos.x >= 0 && pos.y < cells.GetLength( 1 ) && pos.y >= 0 )
        cells[pos.x, pos.y].occupied = true;
    }
  }*/

  void WriteCells( Block block )
  {
    for( int i = 0; i < block.rotation[block.rotationID].Length; i++ )
    {
      Vector2Int pos = block.pos + block.rotation[block.rotationID][i];
      if( pos.x < dimension.x && pos.x >= 0 && pos.y < dimension.y && pos.y >= 0 )
      {
        currentState.cells[pos.x, pos.y].occupied = true;
        currentState.cells[pos.x, pos.y].colorID = block.colorID;
      }
    }
  }

  // needed for display-only tilemap (no logic or cell array)
  void SetTilesDisplay( Tilemap tilemap, Block block, Tile[] tiles )
  {
    for( int i = 0; i < block.rotation[block.rotationID].Length; i++ )
    {
      Vector2Int pos = block.pos + block.rotation[block.rotationID][i];
      tilemap.SetTile( new Vector3Int( pos.x, pos.y, 0 ), tiles[block.colorID] );
    }
  }

  public bool IsBlocked( Block block, Vector2Int direction )
  {
    // use the block's internal state
    for( int i = 0; i < block.rotation[block.rotationID].Length; i++ )
    {
      Vector2Int gpos = block.pos + block.rotation[block.rotationID][i] + direction;
      if( gpos.x >= dimension.x || gpos.x < 0 /*|| gpos.y >= dimension.y*/ || gpos.y < 0 || currentState.cells[gpos.x, gpos.y].occupied )
        return true;
    }
    return false;
  }

  public bool IsBlocked( BlockState bs, Vector2Int direction )
  {
    for( int i = 0; i < bs.block.rotation[bs.rot].Length; i++ )
    {
      Vector2Int gpos = bs.pos + bs.block.rotation[bs.rot][i] + direction;
      if( gpos.x >= dimension.x || gpos.x < 0 /*|| gpos.y >= dimension.y*/ || gpos.y < 0 || currentState.cells[gpos.x, gpos.y].occupied )
        return true;
    }
    return false;
  }
}