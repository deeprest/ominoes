﻿// goals:
// stockpile for a 4-liner
// + burn lines
// dig out holes

// technique
// + create fewest holes. tradeoff is towers
// + burn lines. tradeoff is creating hole
// have a place for O, Z, and S


// For every empty cell in consideration, try each square in every block, for every rotation, for both pieces.
// if the block is [placeable](check if the block has a path to the desired position.), do the same for the next block. 
// if that block is also placeable, measure the [fitness](occupied neighbors) for both blocks. store fitness info.
// greatest fitness wins.

// + avoid holes. count down.
// + count neighbors for cohesion? maybe "order" is enough
// + create path, playback. maybe use A*
// + Use a cell grid independent fron the tilemap.
// + allow simulation to run with no timestep.
// + optimize pathing
// + rotate early instead of at the last second.

// TODO AI: dig down to remove holes: avoid stacking above the top-most buried hole
// TODO AI: stockpile: avoid stacking above a specific hole (longest existing?)
// TODO AI: run unique settings with unique seeds to determine best configuration.
// TODO use genetic algorithm to train AI settings

using System;
using System.Collections.Generic;
using Priority_Queue;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Serialization;

[System.Serializable]
[CreateAssetMenu( fileName = "AIController", menuName = "AIController", order = 0 )]
public class AIController : Controller
{
  [Serializable]
  public struct AITechnique
  {
    public string name;
    public int heightThreshold;
    [FormerlySerializedAs( "wHole" )]
    public int hole;
    [FormerlySerializedAs( "wLow" )]
    public int low;
    [FormerlySerializedAs( "wOrder" )]
    public int rowOrder;
    [FormerlySerializedAs( "wColOrder" )]
    public int colOrder;
    [FormerlySerializedAs( "wBust" )]
    public int bust;
    public bool fast;
  }

  public class TopsData
  {
    public int[] tops;
  }

  public class ColumnData
  {
    public int topmostHole;
    public int order;
  }

  public class RowData
  {
    // count the times the row changes from empty cell to occupied cell.
    // the lower the count, the 'more homogenous' the row
    public int order;
  }

  [System.Serializable]
  public class ResultData
  {
    // want more busted lines when not stockpiling, or when watermark to too high.
    public int bustedLines;
    // want fewer holes always
    public int holesCreated;
    // want lower for stockpiling
    public int totalRowOrder;
    public int totalColOrder;
    public int lowestCell;
    public int fitness;
    // block
    public Vector2Int position;
    public int rotationID;
  }

  public bool fast = true;
  public int maxWalkSteps = 100;
  [Range( 1, 30 )]
  public int rotateCountdown = 10;

  // >1 are experimental techniques in development
  public int Method = 0;
  // choose less optimal results
  public int ChooseResult = 0;

  public AITechnique activeTechnique;
  public AITechnique[] techniques;

  // used to analyze potential block positions
  [SerializeReference] ResultData data;
  List<ResultData> resultData = new List<ResultData>();
  List<Vector2Int> topsList = new List<Vector2Int>();

  // use in calculation. cached to avoid reallocation.
  private ColumnData[] columnData;
  private RowData[] rowData;
  // Use arbitrary max for now; to avoid reallocating. Should be based on a design-chosen limit.
  int[] bustedLines = new int[100];
  int bustedLineCount;
  [SerializeField] int highestCell;

  // block movement
  public Node[] path;
  private int pathIndex;
  private int rotcount;

  public override void Initialize( Game game )
  {
    base.Initialize( game );

    columnData = new ColumnData[game.dimension.x];
    for( int x = 0; x < game.dimension.x; x++ )
    {
      columnData[x] = new ColumnData();
      columnData[x].topmostHole = -1;
    }

    rowData = new RowData[game.dimension.y];
    for( int y = 0; y < game.dimension.y; y++ )
    {
      rowData[y] = new RowData();
      rowData[y].order = 0;
    }
  }

  public override void Destruct() { }

  public override void OnNewBlock( Block newBlock )
  {
    data = null;
  }

  public override void UpdateController( ref InputState inputState )
  {
    Profiler.BeginSample( "UpdateController" );
    if( data == null )
    {
      resultData.Clear();
      // get all empty cells above occupied cells; potential block positions.
      Vector2Int[] tops = GetTops();

      highestCell = 0;
      for( int i = 0; i < tops.Length; i++ )
        highestCell = Math.Max( tops[i].y, highestCell );
      for( int i = 0; i < techniques.Length; i++ )
      {
        if( highestCell > techniques[i].heightThreshold )
        {
          activeTechnique = techniques[i];
          break;
        }
      }

      BlockState bsA = new BlockState()
      {
        block = new Block( game.activeBlock ),
        pos = game.activeBlock.pos,
        rot = game.activeBlock.rotationID
      };

      if( Method == 0 )
      {
        // all potential positions and evaluate for fitness
        List<BlockState> pots = new List<BlockState>();
        for( int t = 0; t < tops.Length; t++ )
        for( int rid = 0; rid < bsA.block.rotation.Length; rid++ )
        for( int cellIndex = 0; cellIndex < bsA.block.rotation[rid].Length; cellIndex++ )
        {
          // adjusted by cell offset
          bsA.pos = tops[t] - bsA.block.rotation[rid][cellIndex];
          bsA.rot = rid;
          if( !game.IsBlocked( bsA, Vector2Int.zero ) )
            pots.Add( bsA );
        }
        for( int i = 0; i < pots.Count; i++ )
          resultData.Add( GetNewResultData( pots[i] ) );
      }
      else if( Method == 1 )
      {
#region Method 1

        // TODO AI: Consider both blocks at once when finding the ideal block placement.
        // Consider two blocks at once, the current and the next one "on deck".
        // Find all potential block states (position and rotation) for the first.
        // For each, find all potential blocks states for the second.
        // Take all valid combination pairs and assess fitness.
        // Choose the best.

        BlockState bsB = new BlockState()
        {
          block = new Block( game.nextBlock ),
          pos = game.nextBlock.pos,
          rot = game.nextBlock.rotationID
        };

        // get the potential positions for the active and next block.
        List<BlockState> potsA = new List<BlockState>();
        List<BlockState> potsB = new List<BlockState>();
        Cell[,] cells = new Cell[game.dimension.x, game.dimension.y];
        Cell[,] cellsB = new Cell[game.dimension.x, game.dimension.y];

        GetPotentials( ref bsA, game.currentState.cells, tops, potsA );

        BlockState potA, potB;
        for( int pa = 0; pa < Math.Min( 20, potsA.Count ); pa++ )
        {
          potA = potsA[pa];
          Array.Copy( game.currentState.cells, cells, cells.Length );
          WriteCells( potA, cells );
          int busted = SimulateGameStep( cells );
          int lowestCell = LowestCell( potA );
          GetPotentials( ref bsB, cells, tops, potsB );
          for( int pb = 0; pb < Math.Min( 20, potsB.Count ); pb++ )
          {
            ResultData result = new ResultData();
            potB = potsB[pb];
            Array.Copy( cells, cellsB, cells.Length );
            WriteCells( potB, cellsB );
            Evaluate( cellsB, result );
            result.position = potA.pos;
            result.rotationID = potA.rot;
            result.lowestCell = Math.Min( lowestCell, LowestCell( potB ) );
            result.bustedLines += busted;
            // recalc fitness with newer values
            CalcFitness( result );
            resultData.Add( result );
          }
        }

#endregion
      }
      else if( Method == 3 )
      {
        //optimization attempt
        // TODO optimize
        /*
        // declare at top level
        byte[] ray = new byte[game.dimension.x * game.dimension.y];
        
        // each frame, get the game state
        for( int x = 0; x < game.dimension.x; x++ )
          for( int y = 0; y < game.dimension.y; y++ )
            ray[x + y * game.dimension.x] = (byte)(game.currentState.cells[x, y].occupied? 0x1 : 0x0);
        
        // declare with function
        byte[] blah = new byte[ray.Length];
        
        // for each block in every rotation, at every potential position (tops), use a fresh game board
        ray.CopyTo( blah, 0 );
        */
      }

      resultData.Sort( SortResults );

      int badpath = 0;
      // pathfinding is expensive so do it last
      for( int i = ChooseResult; i < Math.Min( 10, resultData.Count ); i++ )
      {
        // see if there's a path to the target position
        Node[] pathLocal;
        Node start = new Node( game.activeBlock.pos, game.activeBlock.rotationID );
        Node goal = new Node( resultData[i].position, resultData[i].rotationID );
        if( Getpath( bsA, start, goal, out pathLocal ) )
        {
          path = pathLocal;
          pathIndex = 0;
          data = resultData[i];
          game.UpdateUI( game.seqIndex, data.fitness, data.bustedLines, data.holesCreated, data.lowestCell, data.totalRowOrder, data.totalColOrder );
          Vector3[] positions = new Vector3[path.Length];
          for( int j = 0; j < path.Length; j++ )
            positions[j] = new Vector3( path[j].pos.x + 0.5f, path[j].pos.y + 0.5f, 0 );
          game.lineRenderer.positionCount = positions.Length;
          game.lineRenderer.SetPositions( positions );
          break;
        }
        else
        {
          badpath++;
        }
      }

      if( data == null || path == null )
      {
        // log error
        Debug.LogError( "best is null. no path: " + badpath );
        data = null;
        pathIndex = 0;
        path = null;
        return;
      }
    }

    // Move the piece into position.
    if( path == null )
    {
      // THIS SHOULD NOT HAPPEN
      Debug.LogWarning( name + ": path is null but data is not" );
      data = null;
      pathIndex = 0;
      return;
    }

    if( pathIndex >= path.Length )
    {
      Debug.LogWarning( "path index bad" );
      data = null;
      pathIndex = 0;
      path = null;
      return;
    }

    // Increment the path index
    if( game.activeBlock.pos == path[pathIndex].pos && game.activeBlock.rotationID == path[pathIndex].rot )
    {
      // arrived at waypoint, increment
      if( ++pathIndex >= path.Length )
      {
        data = null;
        pathIndex = 0;
        path = null;
        // the block is in position, lock it in!
        inputState.down = true;
        return;
      }
    }

    Vector2Int next = path[pathIndex].pos - game.activeBlock.pos;
    Vector2Int direction = next;
    // sometimes the piece cannot keep up with the path (horizontally)
    while( next.y >= 0 )
    {
      direction = next;
      if( ++pathIndex >= path.Length )
      {
        pathIndex = path.Length - 1;
        break;
      }
      next = path[pathIndex].pos - game.activeBlock.pos;
    }

    if( direction.x > 0 )
      inputState.right = true;
    else if( direction.x < 0 )
      inputState.left = true;
    else if( direction.y < 0 && path[pathIndex].rot == game.activeBlock.rotationID && (activeTechnique.fast || fast) )
      inputState.down = true;

    if( path[pathIndex].rot != game.activeBlock.rotationID )
    {
      // The input handling code requires rotate inputs to be non-consecutive.
      // This also enforces a fair delay (humans cannot rotate every frame, either)
      if( rotcount-- == 0 )
      {
        if( path[pathIndex].rot - game.activeBlock.rotationID > 0 )
          inputState.rotCW = true;
        else
          inputState.rotCCW = true;
        rotcount = rotateCountdown;
      }
    }
    Profiler.EndSample();
  }

  Vector2Int[] GetTops()
  {
    topsList.Clear();
    for( int x = 0; x < game.dimension.x; x++ )
    {
      // the bottom is always "occupied"
      bool previousOccupied = true;
      for( int y = 0; y < game.dimension.y; y++ )
      {
        bool occupied = game.currentState.cells[x, y].occupied;
        if( !occupied && previousOccupied )
          topsList.Add( new Vector2Int( x, y ) );
        previousOccupied = occupied;
      }
    }
    return topsList.ToArray();
  }

  void CalcFitness( ResultData a )
  {
    a.fitness = a.bustedLines * activeTechnique.bust + -a.holesCreated * activeTechnique.hole + -a.lowestCell * activeTechnique.low + -a.totalRowOrder * activeTechnique.rowOrder + -a.totalColOrder * activeTechnique.colOrder;
  }


  ResultData GetNewResultData( BlockState bs )
  {
    ResultData newResultData = new ResultData();
    for( int x = 0; x < game.dimension.x; x++ )
    {
      columnData[x].topmostHole = -1;
      columnData[x].order = 0;
    }
    for( int y = 0; y < game.dimension.y; y++ )
    {
      rowData[y].order = 0;
    }

    // Simulate the busting of lines, and evaluate the resulting state.
    newResultData.position = bs.pos;
    newResultData.rotationID = bs.rot;
    bustedLineCount = 0;
    for( int y = 0; y < game.dimension.y; y++ )
    {
      int count = 0;
      for( int x = 0; x < game.dimension.x; x++ )
      {
        if( WouldPositionBeOccupied( new Vector2Int( x, y ), bs ) )
          count++;
        else
          break;
      }

      if( count == game.dimension.x )
      {
        // keep a list of would-be busted lines.
        bustedLines[bustedLineCount] = y;
        bustedLineCount++;
      }
    }
    newResultData.bustedLines = bustedLineCount;

    // check for new holes
    int newholes = 0;
    for( int c = 0; c < bs.block.rotation[bs.rot].Length; c++ )
    {
      // only check for holes if the cell is NOT on a busted line
      if( WouldBeABustedLine( (bs.block.pos + bs.block.rotation[bs.rot][c]).y ) )
        continue;
      if( !WouldPositionBeOccupied( bs.pos + bs.block.rotation[bs.rot][c] + Vector2Int.down, bs ) )
        newholes++;
    }
    newResultData.holesCreated = newholes;
    int lowestCell = game.dimension.y;
    for( int c = 0; c < bs.block.rotation[bs.rot].Length; c++ )
      lowestCell = Mathf.Max( 0, Mathf.Min( lowestCell, (bs.pos + bs.block.rotation[bs.rot][c]).y ) );
    newResultData.lowestCell = lowestCell;

    // columns
    for( int x = 0; x < game.dimension.x; x++ )
    {
      // the bottom is always "occupied"
      bool previousOccupied = true;
      for( int y = 0; y < game.dimension.y; y++ )
      {
        if( WouldBeABustedLine( y ) )
          continue;
        bool occupied = WouldPositionBeOccupied( new Vector2Int( x, y ), bs );
        if( occupied != previousOccupied )
          columnData[x].order++;
        previousOccupied = occupied;
      }
    }

    // rows
    for( int y = 0; y < game.dimension.y; y++ )
    {
      if( WouldBeABustedLine( y ) )
        continue;
      bool previousOccupied = WouldPositionBeOccupied( new Vector2Int( 0, y ), bs );
      for( int x = 0; x < game.dimension.x; x++ )
      {
        bool occupied = WouldPositionBeOccupied( new Vector2Int( x, y ), bs );
        if( occupied != previousOccupied )
          rowData[y].order++;
        previousOccupied = occupied;
      }
    }
    int totalroworder = 0;
    for( int y = 0; y < game.dimension.y; y++ )
      totalroworder += rowData[y].order;
    newResultData.totalRowOrder = totalroworder;
    int totalcolorder = 0;
    for( int x = 0; x < game.dimension.x; x++ )
      totalcolorder += columnData[x].order;
    newResultData.totalColOrder = totalcolorder;

    CalcFitness( newResultData );
    return newResultData;
  }

  int SortResults( ResultData a, ResultData b )
  {
    Profiler.BeginSample( "Sorting" );
    int am = 0;
    int bm = 0;
    if( a.bustedLines > b.bustedLines ) am += activeTechnique.bust;
    if( b.bustedLines > a.bustedLines ) bm += activeTechnique.bust;
    if( a.holesCreated < b.holesCreated ) am += activeTechnique.hole;
    if( b.holesCreated < a.holesCreated ) bm += activeTechnique.hole;
    if( a.lowestCell < b.lowestCell ) am += activeTechnique.low;
    if( b.lowestCell < a.lowestCell ) bm += activeTechnique.low;
    if( a.totalRowOrder < b.totalRowOrder ) am += activeTechnique.rowOrder;
    if( b.totalRowOrder < a.totalRowOrder ) bm += activeTechnique.rowOrder;
    if( a.totalColOrder < b.totalColOrder ) am += activeTechnique.colOrder;
    if( b.totalColOrder < a.totalColOrder ) bm += activeTechnique.colOrder;
    // -1 means place a before b in the list
    if( am > bm ) return -1;
    if( bm > am ) return 1;
    Profiler.EndSample();
    return 0;
  }

  void GetPotentials( ref BlockState bs, Cell[,] cells, Vector2Int[] tops, List<BlockState> pots )
  {
    int width = cells.GetLength( 0 );
    int height = cells.GetLength( 1 );
    for( int t = 0; t < tops.Length; t++ )
    for( int rid = 0; rid < bs.block.rotation.Length; rid++ )
    for( int cellIndex = 0; cellIndex < bs.block.rotation[rid].Length; cellIndex++ )
    {
      // adjusted by cell offset
      bs.pos = tops[t] - bs.block.rotation[rid][cellIndex];
      bs.rot = rid;
      // IsBlocked
      bool isBlocked = false;
      for( int i = 0; i < bs.block.rotation[bs.rot].Length; i++ )
      {
        Vector2Int gpos = bs.pos + bs.block.rotation[bs.rot][i];
        if( gpos.x >= width || gpos.y >= height || gpos.x < 0 || gpos.y < 0 || cells[gpos.x, gpos.y].occupied )
        {
          isBlocked = true;
          break;
        }
      }
      if( !isBlocked )
        pots.Add( bs );
    }
  }

  void WriteCells( BlockState bs, Cell[,] cells )
  {
    for( int i = 0; i < bs.block.rotation[bs.rot].Length; i++ )
    {
      Vector2Int pos = bs.pos + bs.block.rotation[bs.rot][i];
      if( pos.x < cells.GetLength( 0 ) && pos.x >= 0 && pos.y < cells.GetLength( 1 ) && pos.y >= 0 )
        cells[pos.x, pos.y].occupied = true;
    }
  }

  bool WouldPositionBeOccupied( Vector2Int pos, BlockState state )
  {
    // out of bounds is "occupied"
    if( pos.x < 0 || pos.x >= game.dimension.x || pos.y < 0 || pos.y >= game.dimension.y ) return true;
    // check against world cells
    if( game.currentState.cells[pos.x, pos.y].occupied ) return true;
    // check against the block itself, in the future position
    if( state.block != null )
      for( int i = 0; i < state.block.rotation[state.rot].Length; i++ )
        if( pos == state.pos + state.block.rotation[state.rot][i] )
          return true;
    return false;
  }

  bool WouldBeABustedLine( int cellY )
  {
    for( int i = 0; i < bustedLineCount; i++ )
      if( bustedLines[i] == cellY )
        return true;
    return false;
  }


  bool BlocksOverlap( BlockState a, BlockState b )
  {
    for( int i = 0; i < a.block.rotation[a.rot].Length; i++ )
    for( int i2 = 0; i2 < b.block.rotation[b.rot].Length; i2++ )
      if( (a.pos + a.block.rotation[a.rot][i]) == (b.pos + b.block.rotation[b.rot][i2]) )
        return true;
    return false;
  }

#region pathfinding

  public struct Node : IEquatable<Node>
  {
    public Node( Vector2Int pos, int rot )
    {
      this.pos = pos;
      this.rot = rot;
    }

    public bool Equals( Node other )
    {
      return other.pos == pos && other.rot == rot;
    }

    public Vector2Int pos;
    public int rot;
  }

  SimplePriorityQueue<Node> frontier = new SimplePriorityQueue<Node>();
  Dictionary<Node, Node> came_from = new Dictionary<Node, Node>();
  Dictionary<Node, int> cost_so_far = new Dictionary<Node, int>();
  List<Node> neighbors = new List<Node>();
  List<Node> temppath = new List<Node>();

  bool Getpath( BlockState state, Node start, Node goal, out Node[] path )
  {
    frontier.Clear();
    frontier.Enqueue( start, 0 );
    came_from.Clear();
    came_from.Add( start, default );
    cost_so_far.Clear();
    cost_so_far.Add( start, 0 );
    Node current;
    while( frontier.Count > 0 )
    {
      current = frontier.Dequeue();
      if( current.Equals( goal ) )
        break;
      Node[] nexts = GetNeighbors( state, current );
      for( int i = 0; i < nexts.Length; i++ )
      {
        Node next = nexts[i];
        int new_cost = cost_so_far[current] + GetCost( current, next );
        if( !came_from.ContainsKey( next ) || new_cost < cost_so_far[next] )
        {
          cost_so_far[next] = new_cost;
          frontier.Enqueue( next, new_cost + Heuristic( goal, next ) );
          came_from[next] = current;
        }
      }
    }

    path = Constructpath( start, goal );
    return path != null;
  }

  Node[] Constructpath( Node start, Node goal )
  {
    if( !came_from.ContainsKey( goal ) ) return null;
    Node current = goal;
    temppath.Clear();
    while( !current.Equals( start ) )
    {
      temppath.Add( current );
      current = came_from[current];
    }
    temppath.Add( start );
    temppath.Reverse();
    return temppath.ToArray();
  }

  Node[] GetNeighbors( BlockState state, Node here )
  {
    // prefer the current rotation, to avoid arbitrary/unnecessary rotations.
    // try the current, then left, then right (the block cannot be rotated twice in one frame)
    int[] rots;
    if( state.block.rotation.Length == 1 )
    {
      rots = new int[] {here.rot};
    }
    else if( state.block.rotation.Length == 2 )
    {
      rots = new int[] {here.rot, (here.rot + 1) % state.block.rotation.Length};
    }
    else
    {
      rots = new int[] {here.rot, (here.rot + 1) % state.block.rotation.Length, (here.rot - 1 + state.block.rotation.Length) % state.block.rotation.Length};
    }
    state.pos = here.pos;
    neighbors.Clear();
    for( int i = 0; i < rots.Length; i++ )
    {
      state.rot = rots[i];
      // check if the block can rotate in current position before moving
      if( !game.IsBlocked( state, Vector2Int.zero ) )
      {
        if( !game.IsBlocked( state, Vector2Int.down ) )
          neighbors.Add( new Node( here.pos + Vector2Int.down, state.rot ) );
        if( !game.IsBlocked( state, Vector2Int.right ) )
          neighbors.Add( new Node( here.pos + Vector2Int.right, state.rot ) );
        if( !game.IsBlocked( state, Vector2Int.left ) )
          neighbors.Add( new Node( here.pos + Vector2Int.left, state.rot ) );
      }
    }
    return neighbors.ToArray();
  }

  int GetCost( Node a, Node b )
  {
    int cost = 0;
    // rotation cost
    if( Mathf.Abs( b.rot - a.rot ) > 1 )
      cost += 100;
    else if( Mathf.Abs( b.rot - a.rot ) > 0 )
      cost += 1;
    // prioritize horizontal movement
    if( a.pos.y != b.pos.y )
      cost += 1;
    // if( a.pos.x != b.pos.x && (came_from[a].pos.x == a.pos.x) )
    //   cost += 1;
    return cost;
  }

  int Heuristic( Node a, Node b )
  {
    return Mathf.Abs( a.pos.x - b.pos.x ) + Mathf.Abs( a.pos.y - b.pos.y ) + 10 * Mathf.Abs( b.rot - a.rot );
  }

#endregion

#region Experimental Method 1

  int[] linesToBust = new int[100];

  int SimulateGameStep( Cell[,] cells )
  {
    int width = cells.GetLength( 0 );
    int height = cells.GetLength( 1 );
    int lineCount = 0;
    for( int y = 0; y < height; y++ )
    {
      int count = 0;
      for( int x = 0; x < width; x++ )
      {
        if( cells[x, y].occupied )
          count++;
        else
          break;
      }
      if( count == width )
      {
        linesToBust[lineCount] = y;
        lineCount++;
      }
    }
    for( int i = 0; i < lineCount; i++ )
      Array.Copy( cells, (linesToBust[i] + 1) * width, cells, linesToBust[i] * width, width * height - (linesToBust[i] + 1) * width );
    return lineCount;
  }

  int LowestCell( BlockState bs )
  {
    int lowestCell = int.MaxValue;
    for( int c = 0; c < bs.block.rotation[bs.rot].Length; c++ )
      lowestCell = Mathf.Max( 0, Mathf.Min( lowestCell, (bs.pos + bs.block.rotation[bs.rot][c]).y ) );
    return lowestCell;
  }

  void Evaluate( Cell[,] cells, ResultData outdata )
  {
    int width = cells.GetLength( 0 );
    int height = cells.GetLength( 1 );

    // initialize column and row data
    ColumnData[] coldat;
    RowData[] rowdat;
    coldat = new ColumnData[width];
    for( int x = 0; x < width; x++ )
    {
      coldat[x] = new ColumnData();
      coldat[x].topmostHole = -1;
      coldat[x].order = 0;
    }
    rowdat = new RowData[height];
    for( int y = 0; y < height; y++ )
    {
      rowdat[y] = new RowData();
      rowdat[y].order = 0;
    }
    outdata.bustedLines = SimulateGameStep( cells );
    int newholes = 0;
    for( int x = 0; x < width; x++ )
    for( int y = 0; y < height - 1; y++ )
      if( !cells[x, y].occupied && cells[x, y + 1].occupied )
        newholes++;
    outdata.holesCreated = newholes;
    bool previousOccupied;
    bool occupied;

    // column data
    for( int x = 0; x < width; x++ )
    {
      previousOccupied = true;
      for( int y = 0; y < height; y++ )
      {
        occupied = cells[x, y].occupied;
        if( occupied != previousOccupied )
          coldat[x].order++;
        previousOccupied = occupied;
      }
    }

    // row data
    for( int y = 0; y < height; y++ )
    {
      previousOccupied = cells[0, y].occupied;
      for( int x = 0; x < game.dimension.x; x++ )
      {
        occupied = cells[x, y].occupied;
        if( occupied != previousOccupied )
          rowdat[y].order++;
        previousOccupied = occupied;
      }
    }
    int totalroworder = 0;
    for( int y = 0; y < game.dimension.y; y++ )
      totalroworder += rowdat[y].order;
    outdata.totalRowOrder = totalroworder;
    int totalcolorder = 0;
    for( int x = 0; x < game.dimension.x; x++ )
      totalcolorder += coldat[x].order;
    outdata.totalColOrder = totalcolorder;
    CalcFitness( outdata );
  }

#endregion
}