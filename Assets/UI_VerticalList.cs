﻿using UnityEngine;

[ExecuteInEditMode]
public class UI_VerticalList : MonoBehaviour
{
  private float cacheHash;
  public bool ExecuteInPlayMode = true;

  void Update()
  {
    if( !ExecuteInPlayMode && !Application.isEditor )
      return;

    float hash = 0;
    bool newHash = false;
    RectTransform[] tfs = gameObject.GetComponentsInChildren<RectTransform>();
    for( int i = 0; i < tfs.Length; i++ )
    {
      RectTransform xf = tfs[i];
      if( xf.GetInstanceID() != transform.GetInstanceID() )
        hash += xf.anchoredPosition.x + xf.anchoredPosition.y + xf.anchorMin.x + xf.anchorMin.y +
                xf.anchorMax.x + xf.anchorMax.y + xf.localPosition.x + xf.localPosition.y +
                xf.sizeDelta.x + xf.sizeDelta.y + xf.pivot.x + xf.pivot.y;
      if( xf.GetSiblingIndex() != i )
        newHash = true;
    }
    if( newHash || cacheHash != hash )
    {
      cacheHash = hash;
      float y = 0;
      for( int i = 0; i < tfs.Length; i++ )
      {
        if( tfs[i].GetInstanceID() != transform.GetInstanceID() )
        {
          tfs[i].localPosition = Vector3.down * y;
          y += tfs[i].rect.height;
        }
      }
    }
  }
}