#!/bin/bash

# --if-changed
macos=$(ls -td build/MacOS/* | head -1)
butler push --fix-permissions $macos deeprest/omino:macos

linux=$(ls -td build/Linux/* | head -1);
butler push --fix-permissions $linux deeprest/omino:linux

windows=$(ls -td build/Windows/* | head -1)
butler push $windows deeprest/omino:windows

# zipwebgl=saga2020-webgl.zip
# rm $zipwebgl
# webgl=$(ls -td build/WebGL/Saga* | head -1)
# echo $webgl
# ditto -c -k --sequesterRsrc $webgl $zipwebgl
# butler push $zipwebgl deeprest/saga2020:webgl
